//import apikey
//init telegram bot api
import TelegramBot from 'node-telegram-bot-api';
const token = process.env.GojuuOnGameBot_token || "";
const bot = new TelegramBot(token, { polling: true });
//check romaji
var hepburn = require("hepburn");
//random kana
var randomKana = require('./randomJapaneseKana')

var total: { [key: number]: number } = {};
var score: { [key: number]: number } = {};
var lastword: { [key: number]: string } = {};
var kanaMode: { [key: number]: number } = {};
export function onStart() {
  bot.onText(/.+/, (message) => {
    console.log(message); // for debug
    const chatId = message.chat.id;
    console.log('you say:' + message.text);
    const msg = message.text || "";
    if (msg.startsWith("/")) {
      //bot.sendMessage(chatId,"you say: "+ message.text + " | response: " + "it is command.");
      //switch for command.
      switch (msg) {
        case "/start":
          total[chatId] = 0;
          score[chatId] = 0;
          lastword[chatId] = "";
          kanaMode[chatId] = 0; // kanaMode { 1 : hinagana , 2 : katakana , other : both}
          bot.sendMessage(chatId, "/start_hiragana for Hiragana.\n/start_katakana for Katakana.\n/start_mixing for both.");
          break;
        case "/start_hiragana":
          total[chatId] = 0;
          score[chatId] = 0;
          lastword[chatId] = "";
          kanaMode[chatId] = 1; // kanaMode { 1 : hinagana , 2 : katakana , other : both}
          bot.sendMessage(chatId, "Game Start!");
          sendNewWord(chatId,msg)
          break;
        case "/start_katakana":
          total[chatId] = 0;
          score[chatId] = 0;
          lastword[chatId] = "";
          kanaMode[chatId] = 2; // kanaMode { 1 : hinagana , 2 : katakana , other : both}
          bot.sendMessage(chatId, "Game Start!");
          sendNewWord(chatId,msg)
          break;
        case "/start_mixing":
          total[chatId] = 0;
          score[chatId] = 0;
          lastword[chatId] = "";
          kanaMode[chatId] = 0; // kanaMode { 1 : hinagana , 2 : katakana , other : both}
          bot.sendMessage(chatId, "Game Start!");
          sendNewWord(chatId,msg)
          break;
        case "/end":
          bot.sendMessage(chatId, "Game Over!");
          break;
        default:
          bot.sendMessage(chatId, "系統：未定義指令。")
      }
    } else if (total[chatId] != null && score[chatId] != null) {
      sendNewWord(chatId, msg)
    } else {
      bot.sendMessage(chatId, "Please enter '/start' to select mode first!")
    }
    //bot.sendMessage(chatId, 'you: '+ message.text + '| tcl: 咩事+_+');

  });
function sendNewWord(chatId:number, msg:string ){
  var returnMsg = "";
      console.log("\nlastword：" + lastword[chatId])
      //if there have lastword, make a condition
      if (lastword[chatId]) {

        const romaji = hepburn.fromKana(lastword[chatId])
        if (romaji.replace(" ", "").toUpperCase() == msg.replace(" ", "").toUpperCase()) {
          returnMsg += "You are corrent!\n"
          score[chatId] += 1;
        } else {
          returnMsg += "You are wrong!\nThe Answer: " + romaji + "\n"
        }
        total[chatId] += 1;
        returnMsg += "Scores: " + score[chatId] + "/" + total[chatId] + '\n'
        returnMsg += "Restart game: /start\n"
        returnMsg += "==================\n"
      } else {

      }
      //gen new lastword and store it
      var kanaModeInt = kanaMode[chatId];
      if (kanaModeInt == 1) {
        lastword[chatId] = randomKana.hiragana()
        //console.log(gimei.name().last().hiragana())
      } else if (kanaModeInt == 2) {
        lastword[chatId] = randomKana.katakana()
      } else {
        const r = randomIntInc(1, 2);
        if (r == 1) {
          lastword[chatId] = randomKana.hiragana()
        } else {
          lastword[chatId] = randomKana.katakana()
        }
      }

      returnMsg += 'What is romaji of\"' + lastword[chatId] + '\"?'
      bot.sendMessage(chatId, returnMsg);
}
  //extra function
  function randomIntInc(low: number, high: number) {
    return Math.floor(Math.random() * (high - low + 1) + low);
  }

}