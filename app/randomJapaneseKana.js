'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
//varr
var type_gimei_1 = require("type-gimei");
module.exports = {
    hiragana: function () {
        //Name.nameObj = null
        var arr = type_gimei_1.Name.hiragana().split(" ");
        return randomFromArray(arr);
    },
    katakana: function () {
        var arr = type_gimei_1.Name.katakana().split(" ");
        return randomFromArray(arr);
    }
};
//extra function
function randomIntInc(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
}
function randomFromArray(items) {
    return items[Math.floor(Math.random() * items.length)];
}
//# sourceMappingURL=randomJapaneseKana.js.map